using System;

namespace Unibo.Oop.Utils
{
    public static class Matrix
    {
        public static IMatrix<TElem> Empty<TElem>(int rows, int columns) {
            return new ArrayMatrix<TElem>(rows, columns);
        }
        
        public static IMatrix<TElem> Of<TElem>(int rows, int columns, TElem elem) {
            var matrix = new ArrayMatrix<TElem>(rows, columns);
            matrix.SetAll(elem);
            return matrix;
        }
        
        public static IMatrix<TElem> Fill<TElem>(int rows, int columns, Func<int, int, TElem> generator) {
            var matrix = new ArrayMatrix<TElem>(rows, columns);
            matrix.SetAll((i, j, x) => generator(i, j));
            return matrix;
        }
    }
}