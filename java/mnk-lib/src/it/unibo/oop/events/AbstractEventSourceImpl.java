package it.unibo.oop.events;

import java.util.Collection;

abstract class AbstractEventSourceImpl<T> implements EventSource<T>, EventEmitter<T>  {

    protected abstract Collection<EventListener<T>> getEventListeners();

    @Override
    public void bind(EventListener<T> eventListener) {
        getEventListeners().add(eventListener);
    }

    @Override
    public void unbind(EventListener<T> eventListener) {
        getEventListeners().remove(eventListener);
    }

    @Override
    public void unbindAll() {
        getEventListeners().clear();
    }

    @Override
    public void emit(T data) {
        getEventListeners().forEach(eventListener -> eventListener.onEvent(data));
    }

    @Override
    public EventSource<T> getEventSource() {
        return this;
    }
}
